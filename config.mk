PRODUCT_SOONG_NAMESPACES += \
    vendor/miuicamera

PRODUCT_COPY_FILES += \
    $(call find-copy-subdir-files,*,vendor/miuicamera/proprietary/etc,$(TARGET_COPY_OUT_SYSTEM)/etc) \
    $(call find-copy-subdir-files,*,vendor/miuicamera/proprietary/lib64/libcameraservice.so:$(TARGET_COPY_OUT_SYSTEM)/lib64/libcameraservice.so) \
    $(call find-copy-subdir-files,*,vendor/miuicamera/proprietary/priv-app/MiuiCamera/lib,$(TARGET_COPY_OUT_SYSTEM)/priv-app/MiuiCamera/lib) \
    $(call find-copy-subdir-files,*,vendor/miuicamera/proprietary/system_ext/lib/vendor.qti.hardware.camera.device@1.0.so:$(TARGET_COPY_OUT_SYSTEM_EXT)/lib/vendor.qti.hardware.camera.device@1.0.so)  \
    $(call find-copy-subdir-files,*,vendor/miuicamera/proprietary/system_ext/lib/vendor.qti.hardware.camera.device@2.0.so:$(TARGET_COPY_OUT_SYSTEM_EXT)/lib/vendor.qti.hardware.camera.device@2.0.so) \
    $(call find-copy-subdir-files,*,vendor/miuicamera/proprietary/system_ext/lib/vendor.qti.hardware.camera.device@3.5.so:$(TARGET_COPY_OUT_SYSTEM_EXT)/lib/vendor.qti.hardware.camera.device@3.5.so) \
    $(call find-copy-subdir-files,*,vendor/miuicamera/proprietary/vendor/bin/hw/android.hardware.camera.provider@2.4-service_64:$(TARGET_COPY_OUT_VENDOR)/bin/hw/android.hardware.camera.provider@2.4-service_64) \
    $(call find-copy-subdir-files,*,vendor/miuicamera/proprietary/vendor/lib/android.hardware.camera.provider@2.4-external.so:$(TARGET_COPY_OUT_VENDOR)/lib/android.hardware.camera.provider@2.4-external.so) \
    $(call find-copy-subdir-files,*,vendor/miuicamera/proprietary/vendor/lib/android.hardware.camera.provider@2.4-legacy.so:$(TARGET_COPY_OUT_VENDOR)/lib/android.hardware.camera.provider@2.4-legacy.so) \
    $(call find-copy-subdir-files,*,vendor/miuicamera/proprietary/vendor/lib/hw/android.hardware.camera.provider@2.4-impl.so:$(TARGET_COPY_OUT_VENDOR)/lib/hw/android.hardware.camera.provider@2.4-impl.so) \
    $(call find-copy-subdir-files,*,vendor/miuicamera/proprietary/vendor/lib/vendor.qti.hardware.camera.device@1.0.so:$(TARGET_COPY_OUT_VENDOR)/lib/vendor.qti.hardware.camera.device@1.0.so) \
    $(call find-copy-subdir-files,*,vendor/miuicamera/proprietary/vendor/lib/vendor.qti.hardware.camera.device@2.0.so:$(TARGET_COPY_OUT_VENDOR)/lib/vendor.qti.hardware.camera.device@2.0.so) \
    $(call find-copy-subdir-files,*,vendor/miuicamera/proprietary/vendor/lib/vendor.qti.hardware.camera.device@3.5.so:$(TARGET_COPY_OUT_VENDOR)/lib/vendor.qti.hardware.camera.device@3.5.so) \
    $(call find-copy-subdir-files,*,vendor/miuicamera/proprietary/vendor/lib64/android.hardware.camera.provider@2.4-external.so:$(TARGET_COPY_OUT_VENDOR)/lib64/android.hardware.camera.provider@2.4-external.so) \
    $(call find-copy-subdir-files,*,vendor/miuicamera/proprietary/vendor/lib64/android.hardware.camera.provider@2.4-legacy.so:$(TARGET_COPY_OUT_VENDOR)/lib64/android.hardware.camera.provider@2.4-legacy.so) \
    $(call find-copy-subdir-files,*,vendor/miuicamera/proprietary/vendor/lib64/hw/android.hardware.camera.provider@2.4-impl.so:$(TARGET_COPY_OUT_VENDOR)/lib64/hw/android.hardware.camera.provider@2.4-impl.so) \
    $(call find-copy-subdir-files,*,vendor/miuicamera/proprietary/vendor/lib64/libmiai_portraitsupernight.so:$(TARGET_COPY_OUT_VENDOR)/lib64/libmiai_portraitsupernight.so) \
    $(call find-copy-subdir-files,*,vendor/miuicamera/proprietary/vendor/lib64/vendor.qti.hardware.camera.device@1.0.so:$(TARGET_COPY_OUT_VENDOR)/lib64/vendor.qti.hardware.camera.device@1.0.so) \
    $(call find-copy-subdir-files,*,vendor/miuicamera/proprietary/vendor/lib64/vendor.qti.hardware.camera.device@2.0.so:$(TARGET_COPY_OUT_VENDOR)/lib64/vendor.qti.hardware.camera.device@2.0.so) \
    $(call find-copy-subdir-files,*,vendor/miuicamera/proprietary/vendor/lib64/vendor.qti.hardware.camera.device@3.5.so:$(TARGET_COPY_OUT_VENDOR)/lib64/vendor.qti.hardware.camera.device@3.5.so) \
    $(call find-copy-subdir-files,*,vendor/miuicamera/proprietary/vendor/etc/dsi_j20s_36_02_0a_video_display_mi.xml:$(TARGET_COPY_OUT_VENDOR)/etc/dsi_j20s_36_02_0a_video_display_mi.xml) \
    $(call find-copy-subdir-files,*,vendor/miuicamera/proprietary/vendor/etc/dsi_j20s_42_02_0b_video_display_mi.xml:$(TARGET_COPY_OUT_VENDOR)/etc/dsi_j20s_42_02_0b_video_display_mi.xml) \
    $(call find-copy-subdir-files,*,vendor/miuicamera/proprietary/vendor/etc/hbtp/hbtpcfg_sdm855_801s_4k.dat:$(TARGET_COPY_OUT_VENDOR)/etc/hbtp/hbtpcfg_sdm855_801s_4k.dat) \
    $(call find-copy-subdir-files,*,vendor/miuicamera/proprietary/vendor/etc/hbtp/loader.cfg:$(TARGET_COPY_OUT_VENDOR)/etc/hbtp/loader.cfg) \
    $(call find-copy-subdir-files,*,vendor/miuicamera/proprietary/vendor/etc/hbtp/qtc801s.bin:$(TARGET_COPY_OUT_VENDOR)/etc/hbtp/qtc801s.bin) \
    $(call find-copy-subdir-files,*,vendor/miuicamera/proprietary/vendor/etc/screen_light.xml:$(TARGET_COPY_OUT_VENDOR)/etc/screen_light.xml) \
    $(call find-copy-subdir-files,*,vendor/miuicamera/proprietary/vendor/firmware/NT36xxx_MP_Setting_Criteria_594A.csv:$(TARGET_COPY_OUT_VENDOR)/firmware/NT36xxx_MP_Setting_Criteria_594A.csv) \
    $(call find-copy-subdir-files,*,vendor/miuicamera/proprietary/vendor/firmware/NT36xxx_MP_Setting_Criteria_594B.csv:$(TARGET_COPY_OUT_VENDOR)/firmware/NT36xxx_MP_Setting_Criteria_594B.csv) \
    $(call find-copy-subdir-files,*,vendor/miuicamera/proprietary/vendor/lib64/camera.device@1.0-impl.so:$(TARGET_COPY_OUT_VENDOR)/lib64/camera.device@1.0-impl.so) \
    $(call find-copy-subdir-files,*,vendor/miuicamera/proprietary/vendor/lib64/camera.device@3.2-impl.so:$(TARGET_COPY_OUT_VENDOR)/lib64/camera.device@3.2-impl.so) \
    $(call find-copy-subdir-files,*,vendor/miuicamera/proprietary/vendor/lib64/camera.device@3.3-impl.so:$(TARGET_COPY_OUT_VENDOR)/lib64/camera.device@3.3-impl.so) \
    $(call find-copy-subdir-files,*,vendor/miuicamera/proprietary/vendor/lib64/camera.device@3.4-external-impl.so:$(TARGET_COPY_OUT_VENDOR)/lib64/camera.device@3.4-external-impl.so) \
    $(call find-copy-subdir-files,*,vendor/miuicamera/proprietary/vendor/lib64/camera.device@3.4-impl.so:$(TARGET_COPY_OUT_VENDOR)/lib64/camera.device@3.4-impl.so) \
    $(call find-copy-subdir-files,*,vendor/miuicamera/proprietary/vendor/lib64/camera.device@3.5-external-impl.so:$(TARGET_COPY_OUT_VENDOR)/lib64/camera.device@3.5-external-impl.so) \
    $(call find-copy-subdir-files,*,vendor/miuicamera/proprietary/vendor/lib64/camera.device@3.5-impl.so:$(TARGET_COPY_OUT_VENDOR)/lib64/camera.device@3.5-impl.so) \
    $(call find-copy-subdir-files,*,vendor/miuicamera/proprietary/vendor/lib64/camera.device@3.6-external-impl.so:$(TARGET_COPY_OUT_VENDOR)/lib64/camera.device@3.6-external-impl.so) \
    $(call find-copy-subdir-files,*,vendor/miuicamera/proprietary/vendor/lib64/libhbtpclient.so:$(TARGET_COPY_OUT_VENDOR)/lib64/libhbtpclient.so) \
    $(call find-copy-subdir-files,*,vendor/miuicamera/proprietary/vendor/lib64/libhbtpdsp.so:$(TARGET_COPY_OUT_VENDOR)/lib64/libhbtpdsp.so) \
    $(call find-copy-subdir-files,*,vendor/miuicamera/proprietary/vendor/lib64/libhbtpfrmwk.so:$(TARGET_COPY_OUT_VENDOR)/lib64/libhbtpfrmwk.so) \
    $(call find-copy-subdir-files,*,vendor/miuicamera/proprietary/system/etc/permissions/vendor.xiaomi.hardware.misys-V1.0-java-permission.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/permissions/vendor.xiaomi.hardware.misys-V1.0-java-permission.xml) \
    $(call find-copy-subdir-files,*,vendor/miuicamera/proprietary/system/etc/permissions/vendor.xiaomi.hardware.misys-V2.0-java-permission.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/permissions/vendor.xiaomi.hardware.misys-V2.0-java-permission.xml) \
    $(call find-copy-subdir-files,*,vendor/miuicamera/proprietary/system/etc/permissions/vendor.xiaomi.hardware.misys.V3_0-permission.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/permissions/vendor.xiaomi.hardware.misys.V3_0-permission.xml) \
    $(call find-copy-subdir-files,*,vendor/miuicamera/proprietary/system/framework/vendor.xiaomi.hardware.misys-V1.0-java.jar:$(TARGET_COPY_OUT_SYSTEM)/framework/vendor.xiaomi.hardware.misys-V1.0-java.jar) \
    $(call find-copy-subdir-files,*,vendor/miuicamera/proprietary/system/framework/vendor.xiaomi.hardware.misys-V2.0-java.jar:$(TARGET_COPY_OUT_SYSTEM)/framework/vendor.xiaomi.hardware.misys-V2.0-java.jar) \
    $(call find-copy-subdir-files,*,vendor/miuicamera/proprietary/system/framework/vendor.xiaomi.hardware.misys.V3_0.jar:$(TARGET_COPY_OUT_SYSTEM)/framework/vendor.xiaomi.hardware.misys.V3_0.jar) \
    $(call find-copy-subdir-files,*,vendor/miuicamera/proprietary/system/vendor/bin/hw/vendor.xiaomi.hardware.misys@1.0-service:$(TARGET_COPY_OUT_VENDOR)/bin/hw/vendor.xiaomi.hardware.misys@1.0-service) \
    $(call find-copy-subdir-files,*,vendor/miuicamera/proprietary/system/vendor/bin/hw/vendor.xiaomi.hardware.misys@2.0-service:$(TARGET_COPY_OUT_VENDOR)/bin/hw/vendor.xiaomi.hardware.misys@2.0-service) \
    $(call find-copy-subdir-files,*,vendor/miuicamera/proprietary/system/vendor/bin/hw/vendor.xiaomi.hardware.misys@3.0-service:$(TARGET_COPY_OUT_VENDOR)/bin/hw/vendor.xiaomi.hardware.misys@3.0-service) \
    $(call find-copy-subdir-files,*,vendor/miuicamera/proprietary/system/vendor/etc/init/vendor.xiaomi.hardware.misys@1.0-service.rc:$(TARGET_COPY_OUT_VENDOR)/etc/init/vendor.xiaomi.hardware.misys@1.0-service.rc) \
    $(call find-copy-subdir-files,*,vendor/miuicamera/proprietary/system/vendor/etc/init/vendor.xiaomi.hardware.misys@2.0-service.rc:$(TARGET_COPY_OUT_VENDOR)/etc/init/vendor.xiaomi.hardware.misys@2.0-service.rc) \
    $(call find-copy-subdir-files,*,vendor/miuicamera/proprietary/system/vendor/etc/init/vendor.xiaomi.hardware.misys@3.0-service.rc:$(TARGET_COPY_OUT_VENDOR)/etc/init/vendor.xiaomi.hardware.misys@3.0-service.rc) \
    $(call find-copy-subdir-files,*,vendor/miuicamera/proprietary/vendor/lib/hw/vendor.xiaomi.hardware.misys@1.0-impl.so:$(TARGET_COPY_OUT_VENDOR)/lib64/hw/vendor.xiaomi.hardware.misys@1.0-impl.so) \
    $(call find-copy-subdir-files,*,vendor/miuicamera/proprietary/vendor/lib/hw/vendor.xiaomi.hardware.misys@2.0-impl.so:$(TARGET_COPY_OUT_VENDOR)/lib64/hw/vendor.xiaomi.hardware.misys@2.0-impl.so) \
    $(call find-copy-subdir-files,*,vendor/miuicamera/proprietary/vendor/lib/hw/vendor.xiaomi.hardware.misys@3.0-impl.so:$(TARGET_COPY_OUT_VENDOR)/lib64/hw/vendor.xiaomi.hardware.misys@3.0-impl.so) \
    $(call find-copy-subdir-files,*,vendor/miuicamera/proprietary/vendor/lib/libcheckpid.so:$(TARGET_COPY_OUT_VENDOR)/lib64/libcheckpid.so) \
    $(call find-copy-subdir-files,*,vendor/miuicamera/proprietary/vendor/lib/liblogwrap_vendor.so:$(TARGET_COPY_OUT_VENDOR)/lib64/liblogwrap_vendor.so) \
    $(call find-copy-subdir-files,*,vendor/miuicamera/proprietary/vendor/lib/vendor.xiaomi.hardware.misys@1.0.so:$(TARGET_COPY_OUT_VENDOR)/lib64/vendor.xiaomi.hardware.misys@1.0.so) \
    $(call find-copy-subdir-files,*,vendor/miuicamera/proprietary/vendor/lib/vendor.xiaomi.hardware.misys@2.0.so:$(TARGET_COPY_OUT_VENDOR)/lib64/vendor.xiaomi.hardware.misys@2.0.so) \
    $(call find-copy-subdir-files,*,vendor/miuicamera/proprietary/vendor/lib/vendor.xiaomi.hardware.misys@3.0.so:$(TARGET_COPY_OUT_VENDOR)/lib64/vendor.xiaomi.hardware.misys@3.0.so) \
    $(call find-copy-subdir-files,*,vendor/miuicamera/proprietary/system/system_ext/etc/permissions/audiosphere.xml:$(TARGET_COPY_OUT_SYSTEM_EXT)/etc/permissions/audiosphere.xml) \
    $(call find-copy-subdir-files,*,vendor/miuicamera/proprietary/system/system_ext/framework/audiosphere.jar:$(TARGET_COPY_OUT_SYSTEM_EXT)/framework/audiosphere.jar) \
    $(call find-copy-subdir-files,*,vendor/miuicamera/proprietary/system/vendor/lib/soundfx/libmisoundfx.so:$(TARGET_COPY_OUT_VENDOR)/lib/soundfx/libmisoundfx.so)

PRODUCT_PACKAGES += \
    MiuiCamera \
    vendor.xiaomi.hardware.misys@1.0 \
    vendor.xiaomi.hardware.misys@2.0 \
    vendor.xiaomi.hardware.misys@3.0

PRODUCT_PRODUCT_PROPERTIES += \
    ro.com.google.lens.oem_camera_package=com.android.camera

include vendor/miuicamera/BoardConfigVendor.mk
